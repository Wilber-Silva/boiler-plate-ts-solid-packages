import { ILogger } from '@wsc-company/types';
import { BaseCrudService } from '@wsc-company/service'
import { FactoryRepository } from '@wsc-company/repositories'

export class ExampleService extends BaseCrudService{
  /**
   * Method to construct instance of Group Service
   *
   * @return void
   */
  constructor(
    _logger: ILogger
  ) {
    super(
      _logger,
      FactoryRepository.getExampleRepository()
    )
  }
}
