import { ILogger } from '@wsc-company/types';
import { ExampleService } from './'

export class Factory {
  public static getExampleService(logger: ILogger): ExampleService {
    return new ExampleService(logger)
  }
}
