import { BaseController } from '@wsc-company/service'
import { Factory as LoggerFactory } from '@wsc-company/logger'
import { ILogger } from '@wsc-company/types/dist'
import express, * as fexpress from 'express'

import { ExampleController } from './'

const { LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE } = process.env

/**
 * Logger to log application
 * @param ILogger
 */
const logger: ILogger = LoggerFactory.getLoggerLog4Js(
  LOGGER_LEVEL,
  LOGGER_TYPE,
  LOGGER_FILE,
)
/**
 * Routes to parse rotes of Express
 * @param express.Router
 */
const router: express.Router = fexpress.Router()

export class Factory {
  public static getExampleController(): ExampleController {
    return new ExampleController(logger, router)
  }
  public static getAll(): BaseController[] {
    return [
      this.getExampleController()
    ]
  }
}
