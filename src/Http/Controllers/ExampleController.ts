import { BaseController } from '@wsc-company/service'
import { IController, ILogger, IRouter } from '@wsc-company/types'
import * as Joi from 'joi'
import { Factory as FactoryService } from './../../Services'

export class ExampleController extends BaseController implements IController {
  /**
   * Logger to log application
   * @param ILogger
   */
  protected _logger: ILogger

  /**
   * Method to construct instance of Users Controller
   *
   * @param _logger Instance of logger
   * @param expressRouter Instance of Express Router
   */
  constructor(
    /**
     * Logger to log application
     * @param ILogger
     */
    logger: ILogger,

    /**
     * Instance of Router
     * @param IRouter
     */
    router: IRouter,
  ) {
    super(
      logger,
      router,
      'Example',
      '/example',
      FactoryService.getExampleService(logger)
    )
    this._logger = logger
    this.initRoutes()
  }

  public initRoutes = (): void => {
    this.router.post(this.path, this.create.bind(this))
  }

  public validationSchema = (): any => {
    const create = Joi.object().keys({
      name: Joi.string().required(),
    })
    return {
      create
    }
  }
}
