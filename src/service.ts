import { Factory as ApplicationFactory } from '@wsc-company/core'
import { Factory as LoggerFactory } from '@wsc-company/logger'
import { Factory as ServiceFactory } from '@wsc-company/service'
import { IApplication, ILogger } from '@wsc-company/types'
import * as express from 'express'
import { Controllers } from './Http'

const { LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE, SERVER_PORT } = process.env

// get logger to log application
const logger: ILogger = LoggerFactory.getLoggerLog4Js(
  LOGGER_LEVEL,
  LOGGER_TYPE,
  LOGGER_FILE,
)

// list of middlewares
const middlewares = []

// list of controllers
const controllers = [...Controllers]

// arguments to service http
const args = {
  port: SERVER_PORT,
}

// get instance of application
const application: IApplication = ApplicationFactory.getApplication(
  'Service Example',
  ServiceFactory.getHandler(logger, express(), middlewares, controllers, args),
  logger,
)

// dispach request to handler
application.dispach()
